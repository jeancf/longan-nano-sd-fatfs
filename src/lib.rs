//! # Purpose
//!
//! This crate provides a driver for use with [FAT FS](https://!crates.io/crates/fatfs) to access and manipulate file systems
//! and files on an SD card on the [**Sipeed Longan Nano**](https://!longan.sipeed.com/en/) board (RISC-V).
//!
//! It is based on the gd32vf103xx-hal crate (implementing embedded-hal).
//! It should therefore be relatively easy to adapt the driver to other platforms implementing embedded-hal.
//!
//! # Usage
//!
//! Refer to  `examples/example.rs` for the full code.
//!
//! Initialize MCU
//!
//! ```rust
//! let peripherals = pac::Peripherals::take().unwrap();
//! let mut rcu = peripherals.RCU.configure().sysclk(108.mhz()).freeze();
//! ```
//!
//! Set up interface with the SD card reader (SPI)
//!
//! ```rust
//! //! Take control of SPI1
//! let spi1 = peripherals.SPI1;
//!
//! //! initialize GPIO pins for SPI1
//! let (sck, miso, mosi, cs) = {
//!     let parts = peripherals.GPIOB.split(&mut rcu);
//!     (
//!         parts.pb13.into_alternate_push_pull(),
//!         parts.pb14.into_floating_input(),
//!         parts.pb15.into_alternate_push_pull(),
//!         parts.pb12.into_push_pull_output(),
//!     )
//! };
//!
//! //! Initialize timer
//! let timer = Timer::timer5(peripherals.TIMER5, 1.hz(), &mut rcu);
//!
//! //! Setup SPI host interface with the card
//! let mut card = Card::new(spi1, cs, sck, miso, mosi, timer, &mut rcu);
//! ```
//!
//! Establish connection with the card that must be inserted in the reader at this point
//!
//! ```rust
//! card.connect().unwrap();
//! ```
//!
//! Select primary partition to access (here, partition `0`) and wrap it in an FsSlice struct
//!
//! ```rust
//! let partition = FsSlice::primary_partition(card, 0).unwrap();
//! ```
//!
//! Initialize file system access to the partition
//!
//! ```rust
//! let fs = fatfs::FileSystem::new(partition, fatfs::FsOptions::new()).unwrap();
//! ```
//!
//! > From here onwards, all the functionalities of FAT FS are available. Check the [FAT FS documentation](https://!docs.rs/fatfs) for details.
//! > The snippets below are just provided for completeness.
//!
//! Write a file in the root directory
//!
//! ```rust
//! let root_dir = fs.root_dir();
//! let mut root_file = root_dir.create_file("rootfile.txt").unwrap();
//! //! Empty file if it already exists
//! root_file.truncate().unwrap();
//! root_file.write_all(b"Hello root World!\n").unwrap();
//! root_file.flush().unwrap();
//!
//! ```
//!
//! Read 18 bytes from a file and print the string to the serial console
//! ```rust
//! let mut rf = root_dir.open_file("rootfile.txt").unwrap();
//! let mut content = [0_u8; 18];
//! let bytes_read = rf.read(&mut content).unwrap();
//! sprintln!(core::str::from_utf8(content.as_ref()).unwrap());
//! ```
//!
//! # Limitations of the driver
//!
//! * The current implementation only supports cards with a Master Boot record (MBR)
//! * Only primary partitions can be accessed (i.e. not extended partitions)
//! * FAT FS supports FAT12, FAT16 and FAT32 partitions
//!
//! *I wrote this driver as a learning exercise in bare-metal development. While the solution is performing as expected, the example.rs binary is ~100 kB large. As the Longan Nano only has 128 kB of flash memory, we are clearly over budget for only the SD card interface functionality. FAT FS is more appropriate for an MCU with more flash available. A barebones crate with minimal functionality (file create/read/write on FAT32) with the smallest possible footprint would be nice to have.*

#![no_std]
use embedded_hal as ehal;
use gd32vf103xx_hal as hal;
use gd32vf103xx_hal::pac;

pub mod card;
pub use card::*;

mod commands;
use commands::*;

mod partitions;
use partitions::*;

pub mod fsslice;
pub use fsslice::*;

pub mod errors;
pub mod traits;

use errors::CardError;
