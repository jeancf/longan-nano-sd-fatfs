use super::*;
use fatfs::{IoBase, Read, Seek, Write};

/// Wrapper around Card object to limit access to single partition on card
/// Shamelessly adapted from fscommon crate to avoid core_io dependency
pub struct FsSlice {
    inner: Card,
    start_offset: u64,
    local_offset: u64,
    size: u64,
}

impl fatfs::IoBase for FsSlice {
    type Error = <Card as fatfs::IoBase>::Error;
}

impl FsSlice {
    /// Create FsSlice based on absolute offset (in bytes) from the start of the card
    pub fn new(
        mut inner: Card,
        start_offset: u64,
        end_offset: u64,
    ) -> Result<Self, <FsSlice as IoBase>::Error> {
        debug_assert!(end_offset >= start_offset);
        inner.seek(fatfs::SeekFrom::Start(start_offset))?;
        let size = end_offset - start_offset + 1;
        Ok(FsSlice {
            inner,
            start_offset,
            local_offset: 0,
            size,
        })
    }

    /// Get an FsSlice wrapping one of the primary partitions of the Card
    /// Valid partition id are [0-3]
    /// Must not be called before connect()
    pub fn primary_partition(
        mut inner: Card,
        part_idx: usize,
    ) -> Result<Self, <FsSlice as IoBase>::Error> {
        if part_idx >= 5 {
            return Err(fatfs::Error::Io(CardError::InvalidPartitionIndex));
        }

        // Panics if called before successful call to connect()
        let partitions = inner.partitions.unwrap();

        // Get the boundaries of the requested partition
        match partitions[part_idx].ptype {
            PartType::FAT12 | PartType::FAT16 | PartType::FAT32 => {
                inner.seek(fatfs::SeekFrom::Start(partitions[part_idx].start_byte))?;
                let size = partitions[0].size;
                Ok(FsSlice {
                    inner,
                    start_offset: partitions[0].start_byte,
                    local_offset: 0,
                    size,
                })
            }
            PartType::Empty => Err(fatfs::Error::Io(CardError::EmptyPartition)),
            PartType::Unsupported => Err(fatfs::Error::Io(CardError::UnsupportPartition)),
        }
    }
}

impl Read for FsSlice {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, <FsSlice as IoBase>::Error> {
        let max_read_size = usize::min((self.size - self.local_offset) as usize, buf.len());
        let bytes_read = self.inner.read(&mut buf[0..max_read_size])?;
        self.local_offset += bytes_read as u64;
        Ok(bytes_read)
    }
}

impl Write for FsSlice {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Self::Error> {
        let max_write_size = usize::min((self.size - self.local_offset) as usize, buf.len());
        let bytes_written = self.inner.write(&buf[0..max_write_size])?;
        self.local_offset += bytes_written as u64;
        Ok(bytes_written)
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        self.inner.flush()?;
        Ok(())
    }
}

impl Seek for FsSlice {
    fn seek(&mut self, pos: fatfs::SeekFrom) -> Result<u64, Self::Error> {
        let new_local_offset = match pos {
            fatfs::SeekFrom::Current(x) => self.local_offset as i64 + x,
            fatfs::SeekFrom::Start(x) => x as i64,
            fatfs::SeekFrom::End(x) => self.size as i64 + x,
        };
        if new_local_offset < 0 || new_local_offset as u64 > self.size - 1 {
            Err(fatfs::Error::Io(CardError::OutOfBounds))
        } else {
            self.inner.seek(fatfs::SeekFrom::Start(
                self.start_offset + new_local_offset as u64,
            ))?;
            self.local_offset = new_local_offset as u64;
            Ok(self.local_offset)
        }
    }
}
