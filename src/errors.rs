use CardError::*;

/// Enum of all the errors that can be emitted by the crate.
/// Is passed to the user as `fatfs::Error::Io(CardError)`
#[derive(Debug)]
pub enum CardError {
    /// Error reported by hardware
    SpiOverrrun,
    /// Error reported by hardware
    SpiModeFault,
    /// Error reported by hardware
    SpiCrc,
    /// Error reported by hardware
    SpiUndefined,
    /// Timed out waiting for ready state
    WaitReadyTimeOut,
    /// Timed out waiting for receive data start flag
    ReceiveBlockTimeOut,
    /// Card requested stop of transmit block
    TransmitBlockStop,
    /// Transmitted block of data was not accepted
    TransmitBlockRejected,
    /// An erase sequence was cleared before executing because an out of erase sequence command was received
    SendCmdEraseReset,
    /// An illegal command code was detected
    SendCmdIllegalCommand,
    /// The CRC check of the last command failed
    SendCmdCrc,
    /// An error in the sequence of erase commands occurred
    SendCmdEraseSequence,
    /// A misaligned address that did not match the block length was used in the command
    SendCmdAddress,
    /// The command's argument was outside the allowed range
    SendCmdParameter,
    /// The response to the command is invalid
    SendCmdInvalidResponse,
    /// The response to SEND_IF_COND command is invalid
    SendIfCondInvalidResponse,
    /// Timed out waiting for ready state after SD_SEND_OP_COND
    SdSendOpCondTimeOut,
    /// The supported voltage of the card is not compatible with the host
    IncompatibleVoltage,
    /// GO_IDLE_STATE could not put the card in Idle mode
    NotIdle,
    /// Offset puts the cursor out of bounds
    OutOfBounds,
    /// Failed to fill whole buffer
    UnexpectedEof,
    /// Failed to write the buffer
    WriteZero,
    /// Cannot read more bytes than buffer size
    BufferOverflow,
    /// No MBR found in sector 0
    MbrNotFound,
    /// Invalid primary partition index (should be 0-3)
    InvalidPartitionIndex,
    /// Attempt to access partition table before it has been read from card
    EmptyPartition,
    /// Partition is not of a supported type
    UnsupportPartition,
}

impl fatfs::IoError for CardError {
    // An error for which `IoError::is_interrupted` returns true is non-fatal
    // and the read operation should be retried if there is nothing else to do.
    fn is_interrupted(&self) -> bool {
        // None of the IO errors are due to a temporary unavailability of data
        false
    }

    // Not sure what to do here
    // Copied from fatfs error.rs
    fn new_unexpected_eof_error() -> Self {
        UnexpectedEof
    }

    // Not sure what to do here
    // Copied from fatfs error.rs
    fn new_write_zero_error() -> Self {
        WriteZero
    }
}

impl core::fmt::Display for CardError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            SpiOverrrun => write!(f, "SpiOverrrun"),
            SpiModeFault => write!(f, "SpiModeFault"),
            SpiCrc => write!(f, "SpiCrc"),
            SpiUndefined => write!(f, "SpiUndefined"),
            WaitReadyTimeOut => write!(f, "WaitReadyTimeOut"),
            ReceiveBlockTimeOut => {
                write!(f, "ReceiveBlockTimeOut")
            }
            TransmitBlockStop => write!(f, "TransmitBlockStop"),
            TransmitBlockRejected => {
                write!(f, "TransmitBlockRejected")
            }
            SendCmdEraseReset => write!(f, "SendCmdEraseReset"),
            SendCmdIllegalCommand => write!(f, "SendCmdIllegalCommand"),
            SendCmdCrc => write!(f, "SendCmdCrc"),
            SendCmdEraseSequence => {
                write!(f, "SendCmdEraseSequence")
            }
            SendCmdAddress => write!(f, "SendCmdAddress"),
            SendCmdParameter => {
                write!(f, "SendCmdParameter")
            }
            SendCmdInvalidResponse => {
                write!(f, "SendCmdInvalidResponse")
            }
            SendIfCondInvalidResponse => {
                write!(f, "SendIfCondInvalidResponse")
            }
            SdSendOpCondTimeOut => {
                write!(f, "SdSendOpCondTimeOut")
            }
            IncompatibleVoltage => write!(f, "IncompatibleVoltage"),
            NotIdle => write!(f, "NotIdle"),
            OutOfBounds => write!(f, "OutOfBounds"),
            UnexpectedEof => write!(f, "UnexpectedEof"),
            WriteZero => write!(f, "WriteZero"),
            BufferOverflow => write!(f, "BufferOverflow"),
            MbrNotFound => write!(f, "MbrNotFound"),
            InvalidPartitionIndex => write!(f, "InvalidPartitionIndex"),
            EmptyPartition => write!(f, "EmptyPartition"),
            UnsupportPartition => write!(f, "UnsupportPartition"),
        }
    }
}
