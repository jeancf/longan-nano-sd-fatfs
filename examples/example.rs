#![no_std]
#![no_main]

use panic_halt as _;
use riscv_rt::entry;

use fatfs::{Read, Write};
use fatfs_sd::Card;
use fatfs_sd::FsSlice;

use hal::{pac, prelude::*, timer::Timer};
use longan_nano::hal;
use longan_nano::sprintln;

#[entry]
fn run() -> ! {
    // Initialize MCU
    let peripherals = pac::Peripherals::take().unwrap();
    let mut rcu = peripherals.RCU.configure().sysclk(108.mhz()).freeze();

    // Initialize serial communication
    let mut afio = peripherals.AFIO.constrain(&mut rcu);
    let gpioa = peripherals.GPIOA.split(&mut rcu);
    longan_nano::stdout::configure(
        peripherals.USART0,
        gpioa.pa9,
        gpioa.pa10,
        115_200.bps(),
        &mut afio,
        &mut rcu,
    );
    sprintln!("Serial interface ready");

    // Initialize interface to SD Card reader
    let spi1 = peripherals.SPI1;

    // initialize GPIO pins for SPI
    let (sck, miso, mosi, cs) = {
        let parts = peripherals.GPIOB.split(&mut rcu);
        (
            parts.pb13.into_alternate_push_pull(),
            parts.pb14.into_floating_input(),
            parts.pb15.into_alternate_push_pull(),
            parts.pb12.into_push_pull_output(),
        )
    };

    // Initialize timer
    let timer = Timer::timer5(peripherals.TIMER5, 1.hz(), &mut rcu);

    // Setup SPI host interface with the card
    let mut card = Card::new(spi1, cs, sck, miso, mosi, timer, &mut rcu);

    // Establish connection with the card (which must be inserted at this point)
    match card.connect() {
        Ok(()) => sprintln!("Card::connect() successful"),
        Err(e) => sprintln!("Card::connect() failed: {}", e),
    };

    // Select partition and wrap it into a slice
    let partition = FsSlice::primary_partition(card, 0).unwrap();

    // Initialize file system struct with FS slice
    let fs = match fatfs::FileSystem::new(partition, fatfs::FsOptions::new()) {
        Ok(fs) => fs,
        Err(e) => {
            sprintln!("Error opening file system {}", e);
            panic!();
        }
    };

    let root_dir = fs.root_dir();

    // write a file in root directory
    let mut root_file = root_dir.create_file("rootfile.txt").unwrap();
    root_file.truncate().unwrap();
    root_file.write_all(b"Hello root World!\n").unwrap();
    root_file.flush().unwrap();
    sprintln!("Created rootfile.txt");

    // write a file in a new directory
    root_dir.create_dir("newdir").unwrap();
    let mut new_file = root_dir.create_file("newdir/newfile.txt").unwrap();
    new_file.truncate().unwrap();
    new_file.write_all(b"Hello New World!\n").unwrap();
    new_file.flush().unwrap();
    sprintln!("Created newdir/newfile.txt");

    // Iterate files in root directory
    for e in root_dir.iter() {
        // Get directory entry
        let de = e.unwrap();
        sprintln!(
            "Entry Name: {:?}",
            core::str::from_utf8(de.short_file_name_as_bytes()).unwrap()
        );
        if de.is_file() {
            // Read file content
            let mut f = de.to_file();
            let mut content = [0 as u8; 17];
            let cnt = f.read(&mut content).unwrap();
            sprintln!("{} bytes read from file", cnt);
            sprintln!(
                "Read: {:?}",
                core::str::from_utf8(content.as_ref()).unwrap()
            );
        } else {
            sprintln!("directory entry is not a file");
        }
    }

    // Open file in directory
    let new_dir = root_dir.open_dir("newdir").unwrap();
    let mut nf = new_dir.open_file("newfile.txt").unwrap();
    // Alternatively
    // let mut nf = root_dir.open_file("newdir/newfile.txt").unwrap();
    sprintln!("newdir/newfile.txt open");

    // Read file content
    let mut content = [0 as u8; 18];
    let cnt = nf.read(&mut content).unwrap();
    sprintln!("{} bytes read from file", cnt);
    sprintln!(
        "Read: {:?}",
        core::str::from_utf8(content.as_ref()).unwrap()
    );

    sprintln!("Execution Finished correctly");
    panic!("Reached the end of the world");
}
